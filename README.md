# Desafio API Weather

##  Introdução
Este projeto foi criado para demonstrar um aplicativo fullstack completo desenvolvido com o MongoDB + Spring boot + Angular 8, utilizando
api de previsões do tempo openweathermap. Incluindo operações CRUD, roteamento, paginação e muito mais.
Para obter mais informações de como api de tempo funciona, acesse o repositório do [openweathermap](https://openweathermap.org/api).

##  Arquitetura Java
A aplicação usa Spring Boot (Web, Angular8).

E o código se organiza em 3 camadas:

1. common, responsável pelas as entidades, negócios e classes de configuração.
2. services, responsável pelo serviços de alto nível para consulta com os objetos de transferência de dados.
3. controller, responsável pelo o recebimento das requisições do client.

##  Banco de dados
Aplicação usa MongoDB no banco de dados, pode ser alterado facilmente no application.properties para qualquer outro banco de dados.

##  Instalação npm
* acesse o link https://nodejs.org/en/ e faça o download.

##  Instalação Angular CLI
* Abra o terminal e digita o comando. 
```
npm install -g @angular/cli
```

##  Execução do módulo api(SpringBoot)
* Faça o download do zip ou clone o repositório Git.
* Descompacte o arquivo zip (se você tiver baixado um).
* Abra o diretório prompt de comando e Altere (cd) para a pasta que contém pom.xml.
* Com o maven já instalado, execute no terminal o comando:
```
mvn clean install
```
* Eclipse aberto.
* Arquivo -> Importar -> Projeto Maven existente -> Navegue até a pasta em que você descompactou o zip.
* Selecione o projeto certo.
* Escolha o arquivo(ApiApplication) do aplicativo de inicialização Spring.
* Clique com o botão direito do mouse no arquivo e em executar como Aplicativo Java.

##  Execução do módulo angular-client(Angular 8)
* Faça o download do zip ou clone o repositório Git.
* Descompacte o arquivo zip (se você tiver baixado um).
* Abra o diretório Prompt de Comando e Altere (cd) para a pasta angular-client.
* Execute os seguintes comando:
```
code .
```
```
npm install
```
```
ng serve
```