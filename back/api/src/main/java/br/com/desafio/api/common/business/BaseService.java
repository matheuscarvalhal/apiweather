package br.com.desafio.api.common.business;

import java.util.List;

public interface BaseService<T> {

	List<T> findAll();

	T findById(Integer id);

	T save(T entity);

	void update(T entity);

	void delete(Integer id);
}