package br.com.desafio.api.common.business;

import br.com.desafio.api.common.entity.Cidade;

public interface CidadeService extends BaseService<Cidade> {
}
