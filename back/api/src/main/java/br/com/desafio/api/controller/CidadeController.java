package br.com.desafio.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.api.common.business.CidadeService;
import br.com.desafio.api.common.entity.Cidade;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;

	@PostMapping("/cidade")
	public Cidade salvar(@Valid @RequestBody Cidade cidade) {
		return cidadeService.save(cidade);
	}
	
	@PutMapping("/cidade")
	public Cidade atualizar(@Valid @RequestBody Cidade cidade){
		return cidadeService.save(cidade);
	 }
	
	@GetMapping("/cidade")
	public List<Cidade> listar(){
	    return cidadeService.findAll();
	 }
	
	@GetMapping("/cidade/{id}")
	public Cidade get(@PathVariable Integer id){
	    return cidadeService.findById(id);
	 }

}