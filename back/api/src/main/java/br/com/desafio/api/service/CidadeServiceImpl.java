package br.com.desafio.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.api.common.business.CidadeService;
import br.com.desafio.api.common.entity.Cidade;
import br.com.desafio.api.service.repository.CidadeRepository;

@Service
public class CidadeServiceImpl implements CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;

	@Override
	public List<Cidade> findAll() {
		return cidadeRepository.findAll();
	}

	@Override
	public Cidade findById(Integer id) {
		return cidadeRepository.findById(id).orElse(null);
	}

	@Override
	public Cidade save(Cidade entity) {
		return cidadeRepository.save(entity);
	}

	@Override
	public void update(Cidade entity) {
		cidadeRepository.save(entity);
	}

	@Override
	public void delete(Integer id) {
		cidadeRepository.deleteById(id);
	}
	
}