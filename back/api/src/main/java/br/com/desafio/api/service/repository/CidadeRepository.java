package br.com.desafio.api.service.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.desafio.api.common.entity.Cidade;

@Repository
public interface CidadeRepository  extends MongoRepository<Cidade, Integer>  {
}	