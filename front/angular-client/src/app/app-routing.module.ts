import { CidadesComponent } from './cidades/cidades.component';
import { CidadeComponent } from './cidades/cidade/cidade.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'list', pathMatch: 'full'
  },
  { path: '', component: CidadesComponent, data: {} },
  { path: 'add', component: CidadeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
