import { Cidade } from './cidade';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CidadeService {

  private appid = 'eb8b1a9405e659b2ffc78f0a520b1a46';
  private urlWeather = 'http://api.openweathermap.org/data/2.5/forecast/daily?';
  private urlApi = 'http://localhost:8080/desafio/api/v1/cidade';

  constructor(private http: HttpClient) { }

  findByName(nome: string): Observable<any> {
    return this.http.get(`${this.urlWeather}q=${nome}&appid=${this.appid}`);
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.urlWeather}id=${id}&cnt=5&appid=${this.appid}&units=metric&lang=pt_br`);
  }

  save(cidade: Cidade): Observable<any> {
    return this.http.post(`${this.urlApi}`, cidade);
  }

  findAll(): Observable<any> {
    return this.http.get(`${this.urlApi}`);
  }
}
