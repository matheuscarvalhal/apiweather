export class Cidade {
  id: number;
  nome: string;
  coordenadas: [];
  pais: string;

  constructor(id?: number, nome?: string, coordenadas?: [], pais?: string) {
    this.id = id;
    this.nome = nome;
    this.coordenadas = coordenadas;
  }
}
