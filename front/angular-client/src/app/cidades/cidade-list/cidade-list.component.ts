import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Cidade } from './../../cidade';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { CidadeService } from 'src/app/cidade.service';

@Component({
  selector: 'app-cidade-list',
  templateUrl: './cidade-list.component.html',
  styleUrls: ['./cidade-list.component.css']
})
export class CidadeListComponent implements OnInit {

  cidades: Array<Cidade> = new Array<Cidade>();
  pageOfItems: Array<any>;

  constructor(private SpinnerService: NgxSpinnerService, private router: Router, private service: CidadeService, private toastr: ToastrService) { }

  ngOnInit() {
    this.SpinnerService.show();
    this.service.findAll().subscribe(res => {
      this.cidades = res;
      this.SpinnerService.hide();
    });
  }

  gotToDetails(cidade: Cidade) {
    this.router.navigate(['add'], { state: { data: cidade } });
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }


}
