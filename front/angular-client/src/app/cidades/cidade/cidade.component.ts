import { CidadeService } from 'src/app/cidade.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Cidade } from '../../cidade';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-cidade',
  templateUrl: './cidade.component.html',
  styleUrls: ['./cidade.component.css']
})
export class CidadeComponent implements OnInit {

  cidade: Cidade = new Cidade();
  novo = true;
  previsoes: Array<any> = new Array();
  urlImg: string;

  constructor(private SpinnerService: NgxSpinnerService, private service: CidadeService,
    private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    if (history.state.data !== undefined) {
      this.listarPrevisoes();
    }
  }

  gotToList() {
    this.router.navigate(['']);
  }

  onSubmit() {

    if (this.cidade.nome === undefined) {
      this.toastr.warning('Informar o nome da cidade e sigla do país');
      return;
    }

    this.SpinnerService.show();

    this.service.findByName(this.cidade.nome)
      .subscribe(data => {
        this.cidade.id = data.city.id;
        this.cidade.nome = data.city.name;
        this.cidade.pais = data.city.country;
        this.service.save(this.cidade)
          .subscribe(cidade => {
            this.SpinnerService.hide();
            this.toastr.success('Cadastro realizado com sucesso.');
            this.gotToList();
          }, error => console.log(error));
      }, error => {
        this.SpinnerService.hide();
        this.toastr.warning('Cidade não válida na API.');
      });
  }

  listarPrevisoes() {
    this.novo = false;
    this.SpinnerService.show();
    this.service.findById(history.state.data.id).subscribe(data => {
      this.cidade.id = data.city.id;
      this.cidade.nome = data.city.name;
      this.cidade.pais = data.city.country;
      this.cidade.coordenadas = data.city.coord;
      this.previsoes = data.list;
      this.previsoes.forEach((e) => {
        e.dt = moment(new Date(e.dt * 1000)).locale('pt-br').format('LLL');
      });
      this.urlImg = `http://openweathermap.org/images/flags/${data.city.country.toLowerCase()}.png`;
      this.SpinnerService.hide();
    });
  }
}
