import { Cidade } from './../cidade';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { CidadeService } from 'src/app/cidade.service';

@Component({
  selector: 'app-cidades',
  templateUrl: './cidades.component.html',
  styleUrls: ['./cidades.component.css']
})
export class CidadesComponent implements OnInit {

  cidades: Array<Cidade> = new Array<Cidade>();

  constructor(private service: CidadeService, private toastr: ToastrService) { }

  ngOnInit() {

  }

}
